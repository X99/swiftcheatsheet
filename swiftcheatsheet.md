## Variables
### Variable vs. constants
```swift
//this is a constant, thus immutable
let pi:Double = 3.14159

//this is a variable
var a:Int = 42
```

### Swift fundamental types
```swift
var integer:Int = 8
var bool:Bool = True
var float:Float = 8.13				    //32 bits
var double:Double = 9.532899			//64 bits
var string:String = "I'm a string"
```

```swift
//Sames goes for 16, 32 and 64. On 64 bits platformes, Int is an Int64 and UInt is an UInt64.
var 8bitsSignedInteger:Int8
var 8bitsUnsignedInteger:UInt8
var 8bitsUnsignedIntegerMaxValue = UInt8.max
var 8bitsUnsignedIntegerMinValue = UInt8.min
//any overflow will result in an error.
```

### Type inference
Most of the time, type can be inferred from value:
```swift
var integer = 8
var bool = True
```

### Multiple declaration
```swift
var x = 0.0, y = 0.0, z = 0.0
var red, green, blue:Double
```

### Displaying
```swift
print("The value of pi is \(pi)")
```

### Hex/Oct/Bin numbers
```swift
let decNumber = 12
let binNumber = 0b101101101
let octNumber = 0o3221
let hexNumber = 0xDAAE

//floating point numbers
let decDouble = 12.4353
let decDoubleWithExponent = 1.24353e1	//the same number
let hexDouble = 0xC.3p4					//Means 12.3x2^4
```

### Padding/separators
```swift
let paddedDouble = 0000123.456
let oneBillion = 1_000_000_000
let aBigNumber = 1_000_000_000.000_000_001
```

### Conversion
```swift
let simpleInteger = 18
let decimalPart = 0.4289
let doubleNumber = Double(simpleInteger) + decimalPart
let backToInteger = Int(doubleNumber)		//equals 18
```

### Type aliases
```swift
typealias Key = UInt64
var myKey:Key = 0xAEBDCAFC
//min and max properties are propagated
```

### Tuples
```swift
let nameAndHeight = ("Mark", 183)
let anotherNameAndHeight = (name: "Jon", height:178)
//nameAndAge is of type (String, Int)
let (name, height) = nameAndAHeight
//height is now 183 and name is "Mark"
let (onlyTheName, _) = nameAndAge
//height will not be retrieved
print("My height is \(nameAndHeight.1)")
print("My name is \(anotherNameAndHeight.name)")
```

### Optionals
```swift
var optionalInteger:Int? = nil
//nil is not a null pointer, it's a marker to an absence of value
if let possibleFailedConversion = Int("328") {
	//some code
}
// -> if "328" can be converted to an Integer, then affect the result to possibleFailedConversion and execute the code between braces.

let optionalInt:Int? = 42
let thisIntExists:Int = optionalInt!

let assumedInt:Int! = 33
let thisIntExistsToo:Int : assumedInt
```

#### Optional chaining
```swift
class ClassA {
    var anInstanceOfClassB:ClassB?
}

class ClassB {
    var aPropertyOfClassB = 1
}

let a = ClassA()
a.anInstanceOfClassB?.aPropertyOfClassB     //will just return nil
a.anInstanceOfClassB!.aPropertyOfClassB     //will fail because of implicit unwrap of nil

var isotopes = ["H":[1, 2, 3], "He":[3, 4]]
isotopes["H"]?[0]   //1
isotopes["Li"]?[0]  //nil
```

## Basic operators

```swift
1 + 2
3 - 4
6 * 7
10.0 / 2.5				//equals 4.0
9 % 4						//equals 1
"this " + " and that"		//equals "this and that"
let minusTwo = -2
minusTwo += 2				//equals 0
```

```swift
(1, "me") < (2, "you")		//true because 1<2
(2, "me") < (2, "you")		//true because "me"<"you"
```

⚠️ inequality operators can’t compare Bool values!

```swift
let cantPass = (height>150? True:False)
```

### nil-coalescing operator
```swift
a ?? b	//unwraps a is it contains a value, returns b is a is nil.
//exactly the same as:
a != nil? a!:b
```

### Range operators
```swift
a...b				//from a to b, both included
a..<b				//from a to b, a included only
...2				//until 2, 2 included
2...				//from 2, included
..<2				//until 2, 2 excluded
let range = ...5
range.contains(7)		//false
range.contains(4)		//true
range.contains(-1)	//true
```

### Logical operators
```swift
!a			//not
a && b	//and
a || b	//or
```

## Strings
```swift
let aString = "Hey, you"

var anotherString = aString + "!"
anotherString.append("!")

let multilineString = """
	This is a multiline
	string.It spans over
	multiple lines. Indentations are not displayed.
	  However, additional spaces are.
	Long lines can even be broken \
	if needed.
"""

for character in aString {
	print(character)
}

let number = 6
let message = "\(number) times 7 is \(number * 7)"
```

```swift
//String.index corresponds to the position of each Character
var str = "This is a string"

str[str.startIndex]										//"T"
str[str.index(before: str.endIndex)]						//"g"
str[str.index(after: str.startIndex)]					//"h"

let index = str.index(str.startIndex, offsetBy: 8)
str[index]												//"a"

//iterate through all characters
for index in str.indices {
    print(str[index], terminator: "")
}
str.insert("!", at:str.endIndex)							//"This is a string!"
let range = str.index(str.endIndex, offsetBy: -9)..<str.endIndex
str.removeSubrange(range)									//"This is 
str.insert(contentsOf: "the end", at: str.endIndex)		//"This is the end"

let indexOfSpace = str.firstIndex(of: " ") ?? str.endIndex
str[..<indexOfSpace]										//"This"
let substring = String(str[..<indexOfSpace])				//Converts the Substring to a string
```

```swift
"This" == "That"											//false
"These" == "These"										//true
```

```swift
"I'm a string".hasPrefix("I'm")							//true
"I'm a string".hasSuffix("ing")
```

💡Indexes can be used with any class that conforms to the Collection protocol.
⚠️ Substrings are not strings on their own, they are just a part of the original string, evening memory.

### Collection types
#### Arrays
**Maintain order:** yes
**Maintain unicity:** no
**Can handle multiple types at once:** no

```swift
var arrayOfIntegers = [Int]()
arrayOfIntegers.count						//0
arrayOfIntegers.append(2)
arrayOfIntegers = []						//array is now empty, but is still of type [Int]

var arrayOfStrings = Array(repeating: "Hi!", count: 5)
var somePlanets:[String] = ["Earth", "Mars", "Venus"]
somePlanets.isEmpty						//false
somePlanets += ["Jupit"]

somePlanets[3] = "Jupiter"
somePlanets[2...3] = ["Saturn", "Mercury", "Pluto"]		//replace "Venus" and "Jupiter"
somePlanets.insert("Jupiter", at:0)
somePlanets.remove(at:0)

var firstItem = somePlanets[0]
somePlanets.removeLast()

for planet in somePlanets {
	print(planet)
}

for (index, value) in somePlanets.enumerated() {
	print("Item \(index+1) is \(value)")
}
```

#### Sets
**Maintain order:** no
**Maintain unicity:** yes
**Can handle multiple types at once:** no

⚠️ All of the elements muse conform to the `Hashable` protocol (which itself conformes to `Equatable` protocol).

```swift
var someLetters = Set<Character>()

someLetters.count					//0
someLetters.insert("a")			//now contains "a"
someLetters = []					//now empty
someLetters = ["b", "z", "f"]		//now contains 3 elements.
someLetters.isEmpty				//false
someLetters.insert("k")
someLetters.remove("z")			//returns the elements that's been removed or nil if element  cannot be found
someLetters.contains("b")			//true

for letter in someLetters {
	print(letter)
}

let vowels:Set = ["a", "e", "i", "o", "u", "y"]
let consonant:Set = ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "v", "s", "t", "v", "w", "x", "z"]
let myNamesLetters:Set = ["p", "i", "e", "r"]
let aFewVowels:Set = ["a", "i", "u"]
vowels.union(consonant).sorted()                    //vowels OR consonant
vowels.intersection(myNamesLetters).sorted()        //vowels AND myNamesLetters
vowels.symmetricDifference(myNamesLetters).sorted() //vowels XOR myNamesLetters
vowels.subtracting(myNamesLetters).sorted()         //vowels NOT myNamesLetters
vowels == consonant                                 //they are not equals
vowels.isSuperset(of: aFewVowels)                   //all of aFewVowels is in vowels
aFewVowels.isSubset(of: vowels)                     //aFewVowels is a part of vowels
vowels.isStrictSuperset(of: aFewVowels)             //superset, but not equal
aFewVowels.isStrictSubset(of: vowels)               //subset, bot not equal
aFewVowels.isDisjoint(with: consonant)              //they have no values in common
```

#### Dictionaries
Stores associations between keys and values.
**Maintain order:** no
**Maintain unicity:** key must be unique.
**Can handle multiple types at once:** no

```swift
var elements = [Int:String]()               //empty dict of type [Int:String] created. Int is the key, String is the value
elements[1] = "Hydrogen"                    //add a value
elements = [:]                              //empty again, type hasn't changed
elements = [1:"Hydrogen", 2:"Helium", 3:"Lithium", 4:"Beryllium"]
elements.count                              //3
elements.isEmpty                            //false
elements[3]                                 //Lithium
elements[39] = "Ittrium"
elements[39] = "Ytrium"                     //fixes the typo
elements.updateValue("Yttrium", forKey: 39) //another way to update an element
elements[119] = "Unknown"
elements[119] = nil                         //removes the element
if let carbon = elements[6] {               //dict will return nil if element cannot be found
    //some code
    print("carbon = \(carbon)")
} else {
    print("Carbon wasn't found")
}
for (atomicNumber, name) in elements {
    print("Elements number \(atomicNumber)'s name is \(name)")
}
for atomicNumber in elements.keys {
    print("Elements number I know: \(atomicNumber)")
}
for name in elements.values {
    print("Elements names I know: \(name)")
}
let atomicNumbers = [Int](elements.keys)
let names = [String](elements.values)

```

### Misc notes
* Variables names and content can contain unicode.
* Swift is type safe and will check for assignations types mismatch.

## Comments
```swift
//Single line comment
/* Multiple line
comment*/
//MARK: use this to create a mark that's easy to find in XCode
//FIXME: marks a piece of code that needs fixes
//TODO: marks something to do.

```

## Control flow
### if
```swift
if a >= 1 {
	//some code
}

//all the tests are executed, if any fails then the whole if fails.
if i > 42, let convertedInt = Int("42"), a < b {
	//some code
} else if i < 42 {
	//some other code
} else {
	//whatever
}
```

### Switch
```swift
let character:Character = "j"
switch character {
case "a", "A":
    print("the letter A")
case "b"..."k":
    print("a letter between b and k")
default:
    print("some other letter")
}

let somePoint = (1, 1)
switch somePoint {
case (0, 0):
    print("\(somePoint) is at the origin")
case (_, 0):
    print("\(somePoint) is on the x-axis")
case (0, _):
    print("\(somePoint) is on the y-axis")
case (-2...2, -2...2):
    print("\(somePoint) is inside the box")
default:
    print("\(somePoint) is outside of the box")
}

let anotherPoint = (2, 0)
switch anotherPoint {
case (let x, 0):
    print("on the x-axis with an x value of \(x)")
case (0, let y):
    print("on the y-axis with a y value of \(y)")
case let (x, y):
    print("somewhere else at (\(x), \(y))")
}

let yetAnotherPoint = (1, -1)
switch yetAnotherPoint {
case let (x, y) where x == y:
    print("(\(x), \(y)) is on the line x == y")
case let (x, y) where x == -y:
    print("(\(x), \(y)) is on the line x == -y")
case let (x, y):
    print("(\(x), \(y)) is just some arbitrary point")
}
```

### For -in
For how to iterate over a data list, see **Collection types** chapter.

```swift
for i in 1...5 {
    print(i)
}
//if value is not needed
for _ in 1...5 {
    //some code that don't need the index
}
for degree in stride(from: 0, to: 361, by: 45) {
    print(degree)
}
```

### while, repeat-while
```swift
//both will print 1 to 10
var counter = 0
while counter < 10 {
    counter += 1
    print(counter)
}

counter = 0
repeat {
    counter += 1
    print(counter)
} while counter < 10
```

### continue
`continue` resumes to the beginning of the next iteration of a loop
```swift
while true {
    continue
    print("I will never be executed")
}
```

### break
`break` exits the enclosing loop/switch
```swift
while true {
    break
    print("I will never be executed")
}
print("Now I'm out of the loop")
```

### fallthrough
`fallthrough` allows to continue through a switch’s `case` statements:
```swift
var number = 5
switch number {
case 1...10:
    print("Number is between 1 and 10")
    fallthrough
case 1, 3, 5, 7, 9:
    print("and also an odd number")
default:
    break
}
```

### labeled statements
Labeled statement can be applied to any loop or switch.
```swift
firstLoop: while true {
    secondLoop: while 1==1 {
        break firstLoop
        print("I will never be exectuted")
    }
    print("I will never be exectuted neither")
}
```

### guard
```swift
var elements = [1:"Hydrogen", 2:"Helium", 3:"Lithium", 4:"Beryllium"]
guard let carbon = elements[6] else {
    //if carbon's missing in the dict
    return
}
```

### API availability
```swift
if #available(iOS 12, macOS 10.12, *) {
    //APIs are available
} else {
    //fall back code
}
```

### Assertions and preconditions
They happened *at runtime*. If the assertion fails, the program *stops*.
⚠️ assertions are checked only in *debug mode*, preconditions always.

```swift
assert(age < 18, "You aren't old enough")
//or
if age >= 18 {
	//some code
} else {
	assertionFailure("You aren't old enough")
}

//preconditions are written the same way:
precondition(age < 18, "You aren't old enough")
//or
if age >= 18 {
	//some code
} else {
	preconditionFailure("You aren't old enough")
}
```

```swift
fatalError("This hasn't been implemented yet!")
//will stop the execution
```


### Error handling
```swift
func iMayThrowAnError() throws {
	//some code
}

do {
	try iMayThrowAnError()
	//yay, no error!
} catch {
	//hm, something went wrong
}

do {
	try makeASandwich()
	eatSandwich()
} catch SandwichError.outOfCleanDishes {
	washDishes()
} catch SandwichError.missingIngredients(let ingredients) {
	buyGroceries(ingredients)
}
```


## Functions
```swift
//Simple function
func sayHello() {
    print("Hello")
}
//With parameters and return value
func addIntegers(a:Int, b:Int) -> Int {
    return a+b
}
//With multiple return values
func addAndSubstract(a:Int, b:Int) -> (addition:Int, substraction:Int) {
    return (a+b, a-b)
}
let calculus = addAndSubstract(a: 5, b: 19)
print("Results are \(calculus.addition) and \(calculus.substraction)")
//With optional tuple
func mayFail(array:[Int]) -> (min:Int, max:Int)? {
    //if no value can be returned
    return nil
}

//with arguments labels
func concat(this str1:String, withThat str2:String) -> String {
    return str1+str2
}
concat(this: "Hello, ", withThat: "this is me")

//some labels name may be ommited of function name is self-explanatory
func concatThis(_ str1:String, withThat str2:String) -> String {
    return str1+str2
}
concatThis("Hello, ", withThat: "this is me")

//default parameter
func printANumber(_ number:Int = 42) {
    print(number)
}
printANumber(18)
printANumber()

//variadic parameter
func sumOf(_ array:Int...) -> Int {
    var sum = 0
    for i in array {
        sum  += i
    }
    return sum
}
sumOf(1, 4, 7, -2)

//in-out parameters
func swapIntegers(_ a:inout Int, _ b:inout Int) {
    let temp = a
    a = b
    b = temp
}
var anInt = 5
var anotherInt = 9
swapIntegers(&anInt, &anotherInt)
anInt               //9
anotherInt          //5

//function types. The same syntax can be used for return types too
var swapFunction:(inout Int, inout Int) -> () = swapIntegers
func callASwapFunction(_ swapfunction:(inout Int, inout Int) -> ()) {
    var i = 5
    var j = 7
    swapfunction(&i, &j)
    print("i = \(i) and j = \(j)")
}
callASwapFunction(swapFunction)
//nested functions. Functions can be defines from within functions.
func computeMeanAndSum(_ array:[Double]) -> (Double, Double) {
    func computeSum(_ array:[Double]) -> Double {
        var sum = 0.0
        for d in array {
            sum += d
        }
        return sum
    }
    //nested functions can even call each others!
    func computeMean(_ array:[Double]) -> Double {
        return computeSum(array) / Double(array.count)
    }
    
    return (computeMean(array), computeSum(array))
}
computeMeanAndSum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
```

### defer
```swift
//defer executes a statement after the function returns.
func deferExample() {
    defer {
        print("executed after the function returns")
    }
    print("I should be the latest statement!")
}
```

## Closures
```swift
//sorted function, standard syntax
let names = ["Chris", "Alex", "Ewa", "Barry", "Daniella"]
func backwards(s1: String, s2: String) -> Bool {
    return s1 > s2
}
var reversed = names.sorted(by: backwards)
//sorted function, closure syntax
reversed = names.sorted(by: {(s1: String, s2: String) -> Bool in return s1 > s2})
//inferring types from context
//as the closure fits a predefined prototype, all the types can be ommited:
reversed = names.sorted(by: {s1, s2 in return s1 > s2})
//the "return" statement can also be ommited if there are no ambiguities:
reversed = names.sorted(by: {s1, s2 in s1 > s2})
//shorthand argument names can also be provided: $0, $1...:
reversed = names.sorted(by: { $0 > $1 } )
//operator function:
reversed = names.sorted(by: >)
//trailing closures. When the closure is the last argument of the function, its body can be places after the function call
reversed = names.sorted { $0 > $1 }
//another example of trailing closure using Array's "map" function:
let digitNames = [
    0: "Zero", 1: "One", 2: "Two",   3: "Three", 4: "Four",
    5: "Five", 6: "Six", 7: "Seven", 8: "Eight", 9: "Nine"
]
let numbers = [16, 58, 510]
let strings = numbers.map {
    (number) -> String in
    var output = ""
    var number = number
    while number > 0 {
        output = digitNames[number % 10]! + output
        number /= 10
    }
    return output
}
print(strings)             //["OneSix", "FiveEight", "FiveOneZero"]
//capturing values
func makeIncrementor(forIncrement amount: Int) -> () -> Int {
    var runningTotal = 0
    func incrementor() -> Int {
        runningTotal += amount
        return runningTotal
    }
    return incrementor
}
//closures are *reference* types
let incrementByTen = makeIncrementor(forIncrement: 10)
incrementByTen()           //10
incrementByTen()           //20
incrementByTen()           //30
//making another incrementor with its very own values
let incrementBySeven = makeIncrementor(forIncrement: 7)
incrementBySeven()         //7
incrementBySeven()         //14
incrementByTen()           //40
```

### Escaping closures
💡Escaping closures are closures that’s passed as an argument to a function but is executed **after** the function returns.
```swift
import UIKit
func anAnsynchronousProcess(completionClosure: @escaping () -> Void) {
    print("Beginning a long process...")
    
    //closure will be called in 10 seconds, way after the *anAsynchronousProcess* function exits.
    //So the reference to its closure must survive after function returns.
    DispatchQueue.main.asyncAfter(deadline: .now()+10) {
        completionClosure()
    }
    print("Exits function...")
}
anAnsynchronousProcess {
    print("Process finished!")
}
```

### Autoclosures
💡Autoclosures are closures that allows to wrap an expression, without any parameters. Their return values is automatically
```swift
var someElements:[String] = ["Hydrogen", "Helium", "Lithium", "Beryllium"]
//this is an autoclosure. It has no arguments and will return what someElements.remove returns.
let popAnElements = { someElements.remove(at: 0) }
popAnElements()         //returns "Hydrogen"
popAnElements()         //returns "Helium"

//use autoclosure as a regular closure
func getNextElement(elementProvider: () -> String) {
    print(elementProvider())
}
getNextElement(elementProvider: {someElements.remove(at: 0)})

//use autoclosure as is
func getNextElement(elementProvider: @autoclosure () -> String) {
    print(elementProvider())
}
getNextElement(elementProvider: someElements.remove(at: 0))
```

### Enums
```swift
enum SomeElements {
    case hydrogen, helium, lithium, beryllium, boron, carbon
}
print(SomeElements.beryllium)   //prints "beryllium"
//enum name can be ommited if type is infered, especially in switches
var anElement:SomeElements = .carbon
//enums can be used as a collection
enum AFewOtherElements:CaseIterable {
    case uranium, yttrium, oganesson, ytterbium, iron
}
for element in AFewOtherElements.allCases {
    print(element)
}
enum Element {
    case number(Int)
    case symbol(String)
}
var carbon:Element = .number(6)
var nitrogen:Element = .symbol("N")
//enum cases can have a value if value's type is specified.
//Each value must be unique.
//if only the first value is specified, the other ones are infered in increasing order.
enum NobleGases:Int {
    case helium = 2
    case neon = 10
    case argon = 18
    case krypton = 36
    case xenon = 54
    case radon = 86
    case oganesson = 118
}
var radon = NobleGases(rawValue:86)
```

## Structures and classes


|   | Enum | Struct | Class |
| --- | --- | --- | --- |
| Have properties |  | Y | Y |
| Have methods |  | Y | Y |
| Have subscripts |  | Y | Y |
| Have initializers | Y | Y | Y |
| Can be extended |  | Y | Y |
| Can conform to protocol |  | Y | Y |
| Can inherit |  | N | Y |
| Check type through typecast |  | N | Y |
| Have deinitializers | N | N | Y |
| Multiple references to an instance |  | N | Y |
| Reference type (reference is assigned to var) | N | N | Y |
| Value type (copied when assigned to a var) | Y | Y | N |


💡Use struct/enums when possible. [Use classes](https://developer.apple.com/documentation/swift/choosing_between_structures_and_classes) when
- Objective-C compatibility is needed
- Type checking is needed

### Properties
⚠️Classes properties must have an initial value unless they are optional, where structure properties can defer this initialisation to the constructor. Structures thus must use type annotation:
```swift
struct SomeStruct {
    var property:Int
}
var myStructure = SomeStruct(property: 5)

class SomeClass {
    var property:Int = 0
}
var myClass = SomeClass()
```

### Lazy properties
Lazy properties are computed when used for the first time
```swift
//exactly the same for classes
struct SomeStruct {
    lazy var property = {
        return 0.0
    }()
}

//if read only
struct SomeStruct {
    var readOnlyProperty:Int {
        //make some calculations if needed
        return 0
    }
}
```

### Computed properties
```swift
//exactly the same for class
struct SomeStruct {
    var getSetProperty:Int {
        get {
            //make some calculations if needed
            return 0
        }
        //is "newValue" (default name) is ok, then it can be ommited.
        set(newValue) {
            //if getSetProperty new value has an impact on other properties
        }
    }
}
```

### Init properties with closure
```swift
//exactly the same for classes
struct SomeStruct {
    var property:Int = {
        //init the property
        return 0
    }()
}
```

### Observers
```swift
struct SomeStruct {
    var property:Int {
        willSet(newValue) {
            //called before setting the property
        }
        didSet {
            //called after setting the property
        }
    }
}
class SomeClass {
    var property:Int = 0 {
        willSet(newValue) {
            //called before setting the property
        }
        didSet {
            //called after setting the property
        }
    }
}
```

### Reference comparison
⚠️This is class-specific, as only classes are reference types.
```swift
//Reference comparison
class MyClass {
}
let anInstance = MyClass()
let sameInstance = anInstance
anInstance === sameInstance	//true
```

### Static properties
💡Following syntax also useable with methods
```swift
struct SomeStruct {
    static var iWillAlwaysHaveTheSameValue = 42
    static var computedStaticValue:Int {
        return 99
    }
}
class SomeClass {
    static var iWillAlwaysHaveTheSameValue = 42
    //if a static property can be overriden, use "class" instead of "static"
    class var computedStaticOverrideableValue:Int {
        return 99
    }
}
```

### Methods
```swift
//exactly the same for class
struct SomeStruct {
    func compute(this value:Int) -> Void {
        //do some calculations
    }
}
```

### Self keyword/mutating
```swift
struct SomeStruct {
    var property = 0
    //when modifying a reference type from within, use "mutating" keyword
    mutating func compute(this value:Int) -> Void {
        self.property = value
    }
}
class SomeClass {
    var property = 0
    //"mutating" keyword's not needed
    func compute(this value:Int) -> Void {
        self.property = value
    }
}
```

💡you can even replace the whole instance of a structure:
```swift
struct SomeStruct {
    mutating func doSomething() {
        self = SomeStruct()
    }
}
```

### Subscript
💡subscripts can have as much parameters (of any type) as needed
```swift
//exactly the same for class
struct SomeStruct {
    subscript(place:Int) -> Int {
        //return value according to place parameter
        return 0
    }
}
```

### Class inheritance and properties/methods overriding
```swift
class Mother {
    func doSomething() {
        //do something...
    }
    func doSomethingElse() {
        print("doSomethingElse")
    }
    //a "final" method or property cannod be overriden
    final func cannotBeOverriden() {
        //do something
    }
}
class Daughter:Mother {
    //overriden method. Same goes for properties.
    override func doSomethingElse() {
         print("Overriden doSomethingElse")
    }
}
var daughter = Daughter()
//inherits from mother class. Same goes for properties
daughter.doSomething()
//daughter's method is called
daughter.doSomethingElse()
```

### Initializers
⚠️Initializers doesn’t return a value.
💡Optional properties can be left un-initialized
💡Initializers can call each other if needed
💡Initializers can call super class’ initializer using `super.init`

#### Basic initializer
```swift
//exactly the same for classes
struct SomeStruct {
    var aProperty:Bool
    init() {
        //property declaration could also carry the default value
        aProperty = false
    }
}
```

#### Multiple initializers
```swift
//exactly the same for classes and enums
struct SomeStruct {
    var lengthInMeters:Double
    init(fromMeters meters:Double) {
        lengthInMeters = meters
    }
    init(fromInches inches:Double) {
        lengthInMeters = inches * 0.0254
    }
}
```

#### Designated/convenience initialisers
💡Designated initializers are regular initialisers. They init all the properties and eventually call superclass’ init.
💡Convenience initializers are used to help initialisation when, for instance, parameters are numerous:
```swift
class SomeClass {
    var property1:String
    var property2:Double
    var property3:Int
    var property4:Bool
    
    init(_ property1:String, _ property2:Double, _ property3:Int, _ property4:Bool) {
        self.property1 = property1
        self.property2 = property2
        self.property3 = property3
        self.property4 = property4
    }
    
    convenience init() {
        self.init("", 0.0, 0, false)
    }
}
```
⚠️Convenience initializers also exist in structs, although *the convenience keyword’s not allowed*

Rules:
- Designated initializers *must* call their immediate superclass initializer
- Convenience initializers *must* call another initializer from the same class 

#### Failable initializers
Failable initializers returns `nil`when initialization fails and class instance cannot be created
```swift
//exactly the same for classes
struct SomeStruct {
    init?() {
        return nil
    }
}
```

#### Required initializers
Required initializers **must** be implemented in any subclass and also carry the `required`tag.
```swift
class SomeClass {
    required init() {
    }
}
```

#### Deinitializers
```swift
class SomeClass {
    deinit {       
    }
}
```

## Error Handling
⚠️All error classes must conform to the `Error` protocol
💡Enums are particularly suited for this use case
```swift
enum MyCustomError:Error {
    case typeMismatch
    case invalidParameter(parameter:Int)
    case stackOverflow
}

//throwing an error
throw MyCustomError.invalidParameter(parameter: 2)

//Error handling #1: propagation
func iMayThrowAnError(value:Int) throws -> Int {
    if value > 5 {
        //first parameter is invalid
        throw MyCustomError.invalidParameter(parameter: 1)
    }
    return value
}

//Error handling #2: do-catch
do {
    try iMayThrowAnError(value: 6)
} catch MyCustomError.typeMismatch {
    //do something
} catch MyCustomError.invalidParameter(let faultyParameter) {
    print("Parameter \(faultyParameter) caused the error")
} catch {
    //will catch anything that wasn't catched yet
    print("Unexpected error: \(error)")
}

//Error handling #3: handle with optional
let x = try? iMayThrowAnError(value: 10)    //x == nil

//Error handling #4: disable error propagation
let y = try! iMayThrowAnError(value: 4)    //if you know that error WILL NOT be thrown
```

## Type Casting
```swift
class Status {
    var name: String
    init(name: String) { self.name = name }
}

class Gas:Status {
}

class Liquid:Status {
}

var collection = [
    Gas(name: "Carbon dioxyde"),
    Liquid(name: "Water"),
]

for substance in collection {
    if substance is Liquid {
        print("\(substance.name) is a liquid!")
    } else if substance is Gas {
        print("\(substance.name) is a gas!")
    }
}

//"as?" returns nil if downcast fails
for substance in collection {
    if let l = substance as? Liquid {
        print("Liquid")
    } else if let g = substance as? Gas {
        print("Gas")
    }
}

var randomCollection: [AnyObject] = [
    Gas(name: "Radon"),
    Liquid(name: "Ethanol"),
]

//"as!" downcast will crash app if it fails
for item in randomCollection {
    let substanceStatus = item as! Status
    print("\(substanceStatus.self)")
    print("\(substanceStatus.name)")
}

for item in randomCollection as! [Status] {
    print("\(item.name)")
}

//"Any" can represent any kind of object
var groups = [Any]()
groups.append(1.0)
groups.append(1)
groups.append("string")
groups.append(Gas(name: "Dihydrogen"))

for item in groups {
    switch item {
    case let anInt as Int:
        print("\(item) is an int")
    case let aDouble as Double:
        print("\(item) is a double")
    case let aString as String:
        print("\(item) is a string")
    case let aGenre as Status:
        print("\(item) is a Status")
    default:
        print("dunno")
    }
}
```

## Extensions
💡Extensions can add methods, initializers, subscripts, nested types…
```swift
extension Int {
    //extensions can add nested types
    enum Kind {
        case negative, zero, positive
    }
    func kind() -> Kind {
        if self == 0 {
            return .zero
        } else if self > 0 {
            return .positive
        }
        return .negative
    }
    
    //extensions can modify their own value
    mutating func increment() {
        self = self+1
    }
    func isEven() -> Bool {
        return self%2 != 0
    }
    func isOdd() -> Bool {
        return !isEven()
    }
    func times(task:(Int) -> ()) {
        for i in 0..<self {
            task(i)
        }
    }
}

2.isOdd()
3.isEven()
2.times(task: {i in
    print(i)                           // 0, 1
})

var i:Int = 2
i.increment()   //i is now 3

(-1).kind()     //negative
0.kind()        //zero
```

## Protocols
💡Enums, classes and structs can conform to protocols
```swift
protocol Element {
    var name:String { get set }
    var symbol:String { get set }
    var atomicNumber:Int { get set }
    var mass:Double { get }
    
    mutating func decay()
    
}

class Hydrogen:Element {
    var name: String = "Hydrogen"
    var symbol: String = "H"
    var atomicNumber: Int = 1
    var mass: Double = 1.0024

    func decay() {
        //code element decay
    }
}

//protocols are types too!
func fuse(element:Element, withElement:Element) -> Element {
    //do something
}
```

💡They can conform to multiple protocols at once
```swift
protocol Protocol1 {}
protocol Protocol2 {}

class Class:Protocol1, Protocol2 {
    
}
```

💡Protocols can inherit protocols
```swift
protocol Protocol1 {}
protocol Protocol2:Protocol1 {}

class Class:Protocol2 {   
}
```

💡Tip: protocols can be “fused” together into new protocols
```swift
protocol Protocol1 {}
protocol Protocol2 {}
typealias Protocol3 = Protocol1 & Protocol2
```

💡Classes, Enums ans Structs can conform to protocols through extensions
```swift
protocol Protocol {
    var variable:String { get }
}

class Class {
}

extension Class:Protocol {
    var variable: String {
        return ""
    }
}
```

💡By conforming to `AnyObject` protocol, a protocol is made class-only
```swift
protocol Protocol:AnyObject {
}

//Error: Non-class type 'Enum' cannot conform to class protocol 'Protocol'
enum Enum:Protocol {
}
```

### Protocol conformance test
```swift
protocol Protocol {
}

class Class:Protocol {
}

var e = Class()

//protocol conformance test
e is Protocol       //true
if let e2 = e as? Protocol {
    //do something
}
```

### Protocol extension
```swift
protocol Protocol {
}

extension Protocol {
}
```

### Providing default implementation through protocol extension
```swift
protocol Protocol {
}

extension Protocol {
    func doSomething() {
        print("I'm here")
    }
}

class Class:Protocol { 
}

var c = Class()
c.doSomething() //prints "I'm here"
```


## Generics
💡 `<T>`and `<Element>` can in fact be anything you want. Call them `<Foo>` if needed, it’s up to you.
```swift
//Generic function
func swap<T>(_ a: inout T, _ b: inout T) {
    let temp = a
    a = b
    b = temp
}

//Generic function
struct Stack<Element> {
    var items = [Element]()
    mutating func push(_ item:Element) {
        items.append(item)
    }
    mutating func pop() -> Element {
        return items.removeLast()
    }
}
```

### Type constraining
```swift
class AClass {}
protocol AProtocol {}

func aFunction<T:AProtocol, U:AClass>(value1:T, value2: U) {
    //do something
}
```

### Associated types
```swift
//The associated type also has the constraint to be Equatable
protocol StackProtocol {
    associatedtype StackableElement:Equatable
    mutating func push(_ item:StackableElement)
    mutating func pop() -> StackableElement
}
```
