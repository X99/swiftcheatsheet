# SwiftCheatSheet

This cheat sheet is based on Apple's Swift 4.2 book's structure. There might be typos, errors, bad code. If so, please feel free [to file an issue](https://gitlab.com/X99/swiftcheatsheet/issues).

It will evolve based on my use of it, but also on YOURS, so leave comment, criticize, propose edits. I see is as a community project, so go ahead and participate.